/*

@file sample_sensor_node.ino
@data 2019-08-02

Copyright (c) 2019 JiNong, Inc.
All right reserved.

*/

#include <Modbus.h>
#include <ModbusSerial.h>
#include <SoftwareSerial.h>
#include "DHT.h"


/* 
 장비정보 크기 6
 연결제품코드 크기 4
 노드 상태 크기 2
*/

#define processDeviceInfo 6
#define processConnectedProduct 4
#define processNodeStatus 2


/*
  DHT22 사용법
  1번핀 +5V, 2번핀 DHTPIN, 4번핀 GROUND
  dht22(온습도센서) 연결 핀 번호 = 5
*/
#define DHTPIN 5    
#define DHTTYPE DHT22 

/*
 BAUD = 9600, Slave ID = 1, TXPIN =8
*/
#define BAUD 9600
#define ID 1
#define TXPIN 8

DHT dht(DHTPIN, DHTTYPE);

ModbusSerial mb;

void setup() {

    dht.begin();
    
    pinMode(5, INPUT); 

    mb.config(&Serial, BAUD, TXPIN); 
    mb.setSlaveId(ID);  

/**
 2~7번까지 각각 회사코드, 제품타입(노드:0, 양액기:1), 제품코드, 프로토콜 버전, 연결장비수, 구역수를 출력
*/
    uint16_t devinfo[6] = {1, 1, 1, 101, 2, 0};
        for (int i = 2; i < processDeviceInfo+2; ++i) {
            mb.addHreg(i,devinfo[i-2]);
        }

/** 
 101~102 번까지 장치코드를 출력.
 */
    uint16_t conproduct[4] = {101,102};
    int j=0 ;
    for (int i = 101; i < processConnectedProduct+101; ++i) {
        mb.addHreg(i,conproduct[j++]);
    }

/** 
 201번에 opid, 202번에 노드 상태를 출력.
*/
    uint16_t nodesta[2] = {0,0};
    int k=0 ;
    for (int i = 201; i < processNodeStatus+201; ++i) {
        mb.addHreg(i,nodesta[k++]);
    }
    
 /**
  200번대에 구동기 정보 출력, 300번대에 제어명령을 하기 위해 모드버스 공간 확보
 */

    float h = dht.readHumidity();                   //습도 측정
    float t = dht.readTemperature();                //온도 측정
  
    /*
    센서를 통해 입력받은 float 데이터 type casting한다.
    센서마다 10의 자리가 바뀌고 1,2번에는 센서값, 3번에는 온도 센서 상태를 나타낸다.
    */
    uint16_t *tem = (uint16_t*) &t;
    uint16_t temperatureValue[3] = {*tem, *(tem+1),0};


    uint16_t *hum = (uint16_t*) &h; 
    uint16_t humidityValue[3] =  {*hum,*(hum+1) , 0};

int m=0;
    for(int i = 211 ; i <214 ; i++){
        mb.addHreg(i, temperatureValue[m++]);
    }

    int n=0;
    for(int i = 221 ; i < 224 ; i++){
        mb.addHreg(i, humidityValue[n++]);
    }
    
}     


void loop() {

    mb.task ();

}